# Settings Extended

By installing this library, it allows a more powerful way of saving and storing values.

```lua

local settings = require("scripts/settings_extended/settings_extended/settings.lua")

local data = settings_extended:get_settings()

data["version"] = {
    owner = "Some string",
    version = {major = 1, minor = 0}
}

-- You can access these values later with 
-- data["version"]["major"]
-- or
-- game:get_value("settingsext__version__version__major)
```

## Installation

Add to your `sopa.toml`:
```toml
[dependencies]
settings_extended = "gitlab:zitrussaft1/settings-extended"
```

Then in your `features.lua` or any early script, add the following line:

```lua
require("scripts/settings_extended/settings_extended/load_hook")
```

When launching the game next time, you should be greeted with the message:
```
Info: Settings Extended: Loading settings...
```
