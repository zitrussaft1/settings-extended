-- Test if it is a flatten table from string
local function is_key_of_table(s)
    assert(type(s)=="string", "Bad argument #1 to 'is_flatten_table' (string expected)")
    return s:match"__"
end

-- Test if it is a valid name
local function is_valid_name(name)
	assert(type(name)=="string", "Bad argument #1 to 'is_valid_name' (string expected)")
	return (not not name:match"^%a[%w_]*$") and (not name:find"__")
end

-- Flatten a table to a depth of 1
-- {a={b=1,c=2}} becomes {a__b=1, a__c=2}
local function table_to_depth1(table)
    assert(type(table)=="table", "Bad argument #1 to 'table_to_strings' (table expected)")
    local strings = {}
    for k,v in pairs(table) do
        if type(v)=="table" then
            for k2,v2 in pairs(table_to_depth1(v)) do
                strings[k .. "__" .. k2] = v2
            end
        else
            strings[k] = v
        end
    end
    return strings
end

-- This function creates a metatable that updates game values when a table is modified.
local function create_metatable(path, game)
    local index = {}

    local data_metatable = {
        __index = function(table, key)
            return index[key]
        end,
        __newindex = function(tbl, key, value)
            assert(is_valid_name(key), "Bad argument #2 to '__newindex' (invalid name). Variable names must not contain '__' and must begin with a letter.")
            if type(value) == "table" then
                local t = table_to_depth1(value)

                -- Remove old values
                if type(index[key]) == "table" then
                    for k,v in pairs(index[key]) do
                        game:set_value(path .. "__" .. key .. "__" .. k, nil)
                    end
                else
                    game:set_value(path .. "__" .. key, nil)
                end

                -- Update game values
                for k,v in pairs(t) do
                    game:set_value(path .. "__" .. key .. "__" .. k, v)
                end

                -- Create a new metatable for the table
                local metatable = create_metatable(path .. "__" .. key, game)
                setmetatable(value, metatable)
                index[key] = value
            else
                game:set_value(path .. "__" .. key, value)
                index[key] = value
            end

        end
    }
    return data_metatable
end

-- Convert a table to a string
local function table_print(table)
    assert(type(table)=="table", "Bad argument #1 to 'table_print' (table expected)")
    local strings = ""
    for k,v in pairs(table) do
        if type(v)=="table" then
            strings = strings .. k .. " = {\n" .. table_print(v) .. "}\n"
        else
            strings = strings .. k .. " = " .. tostring(v) .. "\n"
        end
    end
    return strings
end

-- Unflatten a table from a depth of 1
-- {a__b=1, a__c=2} becomes {a={b=1,c=2}}
local function depth1_to_table(table_depth1, prefix, game)
    assert(type(table_depth1)=="table", "Bad argument #1 to 'depth1_to_table' (table expected)")
    if prefix == nil then prefix = "" end
    local table = {}
    setmetatable(table, create_metatable(prefix, game))
    local t
    for k,v in pairs(table_depth1) do
        t = table
        local address, k = k:match("^(.+)__(.+)$")
        for key in address:gmatch("[^__]+") do
            if t[key] == nil then
                t[key] = {}
            end
            t = t[key]
        end
        t[k] = v
    end
    print(table_print(table))
    return table
end

return {
    table_print = table_print,
    table_to_depth1 = table_to_depth1,
    depth1_to_table = depth1_to_table,
    is_key_of_table = is_key_of_table,
}