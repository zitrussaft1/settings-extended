local utils = require("scripts/settings_extended/utils")

local settings = {}

--// Checks whether string name is valid, beginning with a letter and contains only alpha-numeric/underscore characters


local PREFIX = "extsettings"
local PREFIX_LEN = #PREFIX

local data
function settings:init(save_file_contents, game)
    -- Loop through all lines in the save file
    local data_parsed = {}
    for line in save_file_contents:gmatch("[^\n]+") do
        -- Check if the line is a settings line
        if line:sub(1, PREFIX_LEN) == PREFIX then
            local key, value = line:match("^" .. PREFIX .. "__(.+) = (.+)$")
            -- Convert the value to the correct type
            if value == "true" then
                value = true
            elseif value == "false" then
                value = false
            elseif value == "nil" then
                value = nil
            elseif value:gmatch("^%d+$") then
                value = tonumber(value)
            end
            -- Store the value
            data_parsed[key] = value
        end
    end
    data = utils.depth1_to_table(data_parsed, PREFIX, game)
end

function settings:get_settings()
    return data
end


return settings

--[[ Copyright 2020 Llamazing
  []
  [] This program is free software: you can redistribute it and/or modify it under the
  [] terms of the GNU General Public License as published by the Free Software Foundation,
  [] either version 3 of the License, or (at your option) any later version.
  []
  [] It is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
  [] without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
  [] PURPOSE.  See the GNU General Public License for more details.
  []
  [] You should have received a copy of the GNU General Public License along with this
  [] program.  If not, see <http://www.gnu.org/licenses/>.
  ]]

