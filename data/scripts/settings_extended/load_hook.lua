local load = sol.game.load
local settings_extended = require("scripts/settings_extended/settings")

sol.game.load = function(file_name)
    print("Info: Settings Extended: Loading settings...")

    local game = load(file_name)

	local file = sol.file.open(file_name, "r")
    if file then
        settings_extended:init(file:read("*all"), game)
        file:close()
    else
        settings_extended:init("", game)
    end

    return game
end