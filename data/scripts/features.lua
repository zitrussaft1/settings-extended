-- Sets up all non built-in gameplay features specific to this quest.

-- Usage: require("scripts/features")

-- Features can be enabled to disabled independently by commenting
-- or uncommenting lines below.

require("scripts/settings_extended/load_hook")
require("scripts/hud/hud")
require("scripts/menus/dialog_box")

return true
